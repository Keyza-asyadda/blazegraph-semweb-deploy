FROM gcr.io/distroless/java17-debian12
WORKDIR /app
COPY blazegraph.jar .
ENV JDK_JAVA_OPTIONS="-Xmx256m -server -Dcom.bigdata.journal.AbstractJournal.bufferMode=Disk -Dcom.bigdata.journal.AbstractJournal.file=/app/data/blazegraph.jnl -Djetty.port=8080"
EXPOSE 8080

CMD ["blazegraph.jar"]

